/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_writels.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:08:10 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		<dirent.h>
#include		<stdio.h>
#include		<sys/types.h>
#include		<sys/stat.h>
#include		<unistd.h>
#include		<pwd.h>
#include		<grp.h>
#include		<errno.h>
#include		<time.h>

#include		"ft_ls.h"

char				*ft_writeuser(struct stat *stats, char *ret)
{
	struct passwd	*usr;
	struct group	*grp;

	usr = getpwuid(stats->st_uid);
	grp = getgrgid(stats->st_gid);
	ret = ft_addstr(usr->pw_name, ret);
	ret = ft_addchar(1, ret);
	ret = ft_addstr(grp->gr_name, ret);
	ret = ft_addchar(1, ret);
	if (stats->st_size > 0)
		ret = ft_addnbr(stats->st_size, ret);
	else
		ret = ft_addchar('0', ret);
	ret = ft_addchar(1, ret);
	ret = ft_adddate(ctime(&stats->st_ctime), ret);
	ret = ft_addchar(1, ret);
	return (ret);
}

char			*ft_writeprotect(struct stat stats, t_dirent *dir)
{
	int			nb_link;
	char		*ret;

	ret = ft_memalloc(1024 * sizeof(char));
	nb_link = stats.st_nlink;
	if (dir->data->d_type == DT_LNK)
		ft_addstr("l", ret);
	else if (dir->data->d_type == DT_DIR)
		ft_addstr("d", ret);
	else
		ft_addstr("-", ret);
	ret = ft_addchar(MODE(S_IRUSR)? 'r' : '-', ret);
	ret = ft_addchar(MODE(S_IWUSR)? 'w' : '-', ret);
	ret = ft_addchar(MODE(S_IXUSR)? 'x' : '-', ret);
	ret = ft_addchar(MODE(S_IRGRP)? 'r' : '-', ret);
	ret = ft_addchar(MODE(S_IWGRP)? 'w' : '-', ret);
	ret = ft_addchar(MODE(S_IXGRP)? 'x' : '-', ret);
	ret = ft_addchar(MODE(S_IROTH)? 'r' : '-', ret);
	ret = ft_addchar(MODE(S_IWOTH)? 'w' : '-', ret);
	ret = ft_addchar(MODE(S_IXOTH)? 'x' : '-', ret);
	if (dir->data->d_type == DT_FIFO)
		ft_addstr("@", ret);
	else
		ft_addstr(" ", ret);
	ret = ft_addchar(1, ret);
	ret = ft_addnbr(nb_link, ret);
	ret = ft_addchar(1, ret);
	ret = ft_writeuser(&stats, ret);
	return (ret);
}

void			*ft_writeliste(t_dirent *dir, t_info *info)
{
	char		*ret;
	char		*path;
	struct stat	stats;
	int			ret_stat;

	ret = NULL;
	path = ft_getpath(info->files->name, dir->data->d_name, 0);
	if (dir->data->d_type == DT_LNK)
	{
		ret_stat = lstat(path, &stats);
		if (ret_stat >= 0)
			ret = ft_writeprotect(stats, dir);
	}
	else
	{
		ret_stat = stat(path, &stats);
		if (ret_stat >= 0)
			ret = ft_writeprotect(stats, dir);
	}
	return (ret);
}

int				ft_getsizecol(int index, char *str)
{
	int			i;
	int			j;

	++index;
	i = 0;
	j = 0;
	while (str[i] && index > 0)
	{
		if (index == 1)
		{
			while (str[i + j] != 1)
				++j;
			--index;
		}
		if (str[i] == 1)
			--index;
		++i;
	}
	return (j);
}

void			ft_printnamefile(t_info *info, t_dirent *rep)
{
	char		**ret;
	DIR			*lnk;
	t_dirent	*first;
	t_dirent	*lnk_data;
	int			i;

	i = 0;
	lnk = ft_memalloc(1 * sizeof (DIR));
	lnk_data = ft_memalloc(1 * sizeof (t_dirent));
	first = rep;
	ret = ft_memalloc(1024 * sizeof (char **));
	if (info)
		while(rep->data)
		{
			if (info->options[OPTION_L - 1] == '1')
				ret[i] = ft_writeliste(rep, info);
			else
				ret[i] = NULL;
			if (rep->data->d_type == DT_DIR)
				if (info->options[OPTION_RR - 1] == '1')
					info->files = ft_addfileher(info->files, rep->data->d_name, info->files->name);
			rep = rep->next;
			++i;
		}
	ret[i] = NULL;
	ret = ft_columliste(ret);
	i = 0;
	rep = first;
	while (rep->data)
	{
		ft_cleanstr(ret[i++]);
		ft_putstr(rep->data->d_name);
		if (rep->data->d_type == DT_LNK)
			if (info->options[OPTION_L - 1] == '1')
				ft_putstr(" -> ");
		ft_putstr("\n");
		rep = rep->next;
	}
}
