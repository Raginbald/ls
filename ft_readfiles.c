/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readfiles.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:07:55 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		<sys/types.h>
#include		<dirent.h>
#include		<unistd.h>
#include		<errno.h>

#include		"ft_ls.h"

t_dirent		*ft_getdir(const char *dir)
{
	DIR			*ret;
	t_dirent	*drent;
	t_dirent	*first;

	ret = opendir(dir);
	if (ret == NULL)
		return (NULL);
	drent = ft_memalloc(sizeof(t_dirent *));
	drent->data = readdir(ret);
	drent->prev = NULL;
	first = drent;
	while (drent->data)
	{
		drent->next = ft_memalloc(sizeof(t_dirent *));
		drent->next->data = ft_memalloc(sizeof(struct dirent *));
		drent->next->data = readdir(ret);
		drent->next->next = NULL;
		drent->next->prev = drent;
		drent = drent->next;
	}
	closedir(ret);
	return (first);
}

t_dirent			*ft_triefiles(t_info *info, t_dirent *dir)
{
  if (info->options[OPTION_R - 1] == '1')
    dir = ft_invlist(dir);
  if (info->options[OPTION_A - 1] != '1')
    dir = ft_dellhidlist(dir);
  if (info->options[OPTION_T - 1] == '1')
    {
    }
  return (dir);
}
