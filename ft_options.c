/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:07:48 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include			"ft_ls.h"


t_info		*ft_getoptions(t_info *info, int ac, char **av)
{
	int				i;
	int				j;

	i = 0;
	info = ft_cleanoptions(info);
	while (ac > ++i)
	{
		if (ft_isoption(av[i]))
		{
			j = 0;
			while (av[i][++j])
				if (ft_isvalcharoption(av[i][j]))
					info->options[ft_isvalcharoption(av[i][j]) - 1] = '1';
		}
	}
	return (info);
}

t_info			*ft_cleanoptions(t_info *info)
{
	info->options[0] = '0';
	info->options[1] = '0';
	info->options[2] = '0';
	info->options[3] = '0';
	info->options[4] = '0';
	info->options[5] = '\0';
	return (info);
}

int				ft_isvalcharoption(char c)
{
	if (c == 'l')
		return (1);
	else if (c == 'R')
		return (2);
	else if (c == 'a')
		return (3);
	else if (c == 'r')
		return (4);
	else if (c == 't')
		return (5);
	return (0);
}

int				ft_isoption(char const *str)
{
	int			i;
	int			nopt;

	i = 1;
	nopt = 0;
	if (str && str[0] == '-')
	{
		while (str[i])
		{
			if (ft_isvalcharoption(str[i]))
				++nopt;
			++i;
		}
		if (nopt == (ft_strlen(str) - 1))
			return (1);
	}
	return (0);
}

int				ft_haveoption(t_info *info, char c)
{
	if (info->options[(int) c] == '1')
		return (1);
	return (0);
}
