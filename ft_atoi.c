/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:07:02 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"ft_ls.h"

int		ft_atoi(char const *str)
{
	int	mul;
	int	ret;
	int	i;

	i = 0;
	ret = 0;
	while (str[i])
		++i;
	mul = 1;
	while (--i >= 0)
	{
		ret = ret + ((str[i] - '0') * mul);
		mul = mul * 10;
	}
	return (ret);
}
