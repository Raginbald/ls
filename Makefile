#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/15 17:00:32 by graybaud          #+#    #+#              #
#    Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr        #
#                                                                              #
#******************************************************************************#
.PHONY: all $(NAME) clean fclean re
NAME =		ft_ls
SRC =		ft_ls.c			ft_addstr.c		ft_atoi.c	ft_cleanliste.c		\
			ft_files.c		ft_lib.c		ft_list.c	ft_options.c		\
			ft_readfiles.c	ft_writels.c
CFLAGS = 	-Wall -Wextra -Werror
HDPATH =	.

all: $(NAME)

$(NAME): $(SRC)
			gcc $(CFLAGS) -o $(NAME) $(SRC) -I $(HDPATH)
			gcc -g $(CFLAGS) -o debug $(SRC) -I $(HDPATH)

			@echo "relink"

clean:
			rm -rf $(SRC:.c=.c~) $(LIB:.c=.c~) Makefile~

fclean: clean
			rm -rf $(NAME)

re: fclean all
