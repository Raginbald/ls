/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:01:12 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		"ft_ls.h"
#include		<stdio.h>
#include		<stdlib.h>
#include		<dirent.h>

void			ft_printls(t_info *info)
{
	t_dirent	*cur_dir;

	while (info->files)
	{
		ft_putstr(info->files->name);
		ft_putstr(":\n");
		cur_dir = ft_getdir(info->files->name);
		if (cur_dir == NULL)
		{
			ft_putstr("Erreur d ouverture du fichier : ");
			ft_putstr(info->files->name);
			ft_putstr("\n");
		}
		else
		{
			cur_dir = ft_triefiles(info, cur_dir);
			ft_printnamefile(info, cur_dir);
			ft_putstr("\n");
			free(cur_dir);
		}
		info->files = info->files->next;
	}
}

int				ft_ls(int ac, char **av)
{
	int			ret;
	t_info		*info;

	ret = 0;
	info = ft_memalloc((size_t) 1 + sizeof(t_info));
	info = ft_getoptions(info, ac, av);
	info = ft_getfiles(info, ac, av);
	ft_printls(info);
	return (ret);
}

int				main(int ac, char **av)
{
	int			ret;

	ret = ft_ls(ac, av);
	return (ret);
}
