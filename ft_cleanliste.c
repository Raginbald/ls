/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cleanliste.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:07:26 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		"ft_ls.h"

char			**ft_addspace(int size, char **list, int n_col)
{
	int			i;
	int			total;

	i = 0;
	while (list[i])
	{
		if (size >= ft_getsizecol(n_col, list[i]))
		{
			total = size - ft_getsizecol(n_col, list[i]) + 1;
			list[i] = ft_addspacestr(list[i], total, n_col);
		}
		++i;
	}
	return (list);
}

char			*ft_addspacestr(char *str, int nb_space, int index)
{
	int			i;
	int			j;
	char		*ret;

	i = 0;
	j = 0;
	ret = ft_memalloc((ft_strlen(str) + nb_space + 1) * sizeof (char));
	while (index)
	{
		ret[i] = str[i];
		if (str[i] == 1)
			--index;
		++i;
	}
	while (nb_space)
	{
		ret[i + j] = ' ';
		--nb_space;
		++j;
	}
	while (str[i])
	{
		ret[i + j] = str[i];
		++i;
	}
	ret[i + j] = '\0';
	return (ret);
}

void			ft_cleanstr(char *str)
{
	int			i;

	i = 0;
	if (str)
		while (str[i])
		{
			if (str[i] == 1)
				str[i] = ' ';
			++i;
		}
	ft_putstr(str);
}


char			**ft_columliste(char **ret)
{
	int			i;
	int			nb_col;
	int			size;

	i = -1;
	size = 0;
	nb_col = 1;
	while (nb_col < 5)
	{
		while (ret[++i])
			if (size < ft_getsizecol(nb_col, ret[i]))
				size = ft_getsizecol(nb_col, ret[i]);
		ret = ft_addspace(size, ret, nb_col);
		size = 0;
		++nb_col;
		i = 0;
	}
	return (ret);
}
