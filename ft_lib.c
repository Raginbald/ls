/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lib.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:02:25 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"ft_ls.h"

#include	<unistd.h>
#include	<stdlib.h>

char		*ft_getpath(char *s1, char *s2, char opt)
{
	char	*str;
	int		size;
	int		i;
	int		j;

	i = -1;
	j = ft_strlen(s1);
	size = ft_strlen(s1) + ft_strlen(s2) + 3;
	str = ft_memalloc(size * sizeof(char));
	while (s1[++i])
		str[i] = s1[i];
	if (s2[0] != '/' && s1[j - 1] != '/')
		str[i++] = '/';
	j = -1;
	while (s2[++j])
		str[i + j] = s2[j];
	if (opt == 1)
		str[i + j++] = '/';
	str[i + j] = '\0';
	return (str);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int     ft_strcmp(char *s1, char *s2)
{
  int   i;

  i = 0;
  while (s1[i])
    {
      if (s1[i] < s2[i])
        return (-1);
      if (s1[i] > s2[i])
        return (1);
      i++;
    }
  if (s1[i] < s2[i])
    return (-1);
  if (s1[i] > s2[i])
    return (1);
  return (0);
}

void		ft_putstr(char const *str)
{
	int		i;
	int		ret;

	i = 0;
	ret = 0;
	if (str)
	{
		while (str[i])
			++i;
		ret = write(1, str, i);
		if (ret == -1)
			exit(1);
	}
}

void		ft_free(void *ptr)
{
	free(ptr);
}

char		*ft_getstr(const char *str)
{
	int		i;
	char	*ret;

	i = -1;
	ret = ft_memalloc(ft_strlen(str) * sizeof (char));
	while (str[++i])
		ret[i] = str[i];
	ret[i] = '\0';
	return (ret);
}

void		*ft_memalloc(unsigned int size)
{
	void	*ptr;

	ptr = malloc(size);
	if (!ptr)
	{
		ft_putstr("Erreur: ft_memalloc() return (-1)\n");
		exit(1);
	}
	return (ptr);
}

int			ft_strlen(char const *str)
{
	int		i;

	i = 0;
	while (str[i])
		++i;
	return (i);
}
