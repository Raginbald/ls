/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:03:45 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		<unistd.h>
#include		<stdio.h>
#include		<dirent.h>

#include		<stdlib.h>

#include		"ft_ls.h"

t_file			*ft_addfile(t_file *file, char const *file_name)
{
	int			i;
	t_file		*elem;
	t_file		*first;

	i = -1;
	first = file;
	while(file)
		file = file->next;
	elem = ft_memalloc(1 * sizeof(t_file *));
	elem->name = ft_memalloc(ft_strlen(file_name) * sizeof(char));
	while (file_name[++i])
		elem->name[i] = file_name[i];
	elem->name[i] = '\0';
	elem->next = NULL;
	elem->prev = file;
	if (file)
	{
		file->next = elem;
		return (first);
	}
	return (elem);
}

void			*ft_dellhidlist(t_dirent *file)
{
	t_dirent	*tmp;

	tmp = file;
	while (file->next)
    {
		if (file->data->d_name[0] == '.')
		{
			if (file->prev)
				file->prev->next = file->next;
			if (file->next)
				file->next->prev = file->prev;
			tmp = file->next;
		}
		file = file->next;
    }
	return (tmp);
}

void			*ft_invlist(t_dirent *file)
{
	t_dirent	*tmp;

	tmp = ft_memalloc(1 * sizeof(t_dirent *));
	while (file->next)
	{
		tmp->data = file->data;
		tmp->next = file->prev;
		tmp->prev = file->next;
		tmp = tmp->prev;
		file = file->next;
	}
	while (tmp->next)
		tmp = tmp->next;
	return (tmp);
}

t_file		*ft_addfileher(t_file *rep, char *name, char *parent)
{
	t_file	*current;
	int		size;
	int		i;

	i = 0;
	current = rep;
	while (rep->next)
		rep = rep->next;
	size = ft_strlen(name) + ft_strlen(rep->name) + 1;
	rep->next = ft_memalloc(1 * sizeof (t_file *));
	rep->next->next = NULL;
	rep->next->name = ft_memalloc(size * sizeof (char));
	rep->next->name = ft_getpath(parent, name, 1);
	rep->next->prev = rep;
	return (current);
}
