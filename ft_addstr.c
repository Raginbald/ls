/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:09:44 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include			"ft_ls.h"

char			*ft_addchar(char c, char *str)
{
	int			i;

	i = 0;
	while (str[i])
		++i;
	str[i] = c;
	str[i + 1] = '\0';
	return (str);
}

char			*ft_addnbr(int nb, char *str)
{
	int			reverse_nb;
	int			i;

	i = 0;
	reverse_nb = 0;
	while (str[i])
		++i;
	if(nb < 0)
	{
		str[i++] = '-';
		nb = -nb;
	}
	while(nb > 0)
	{
		reverse_nb *= 10;
		reverse_nb += nb % 10;
		nb         /= 10;
	}
	while(reverse_nb > 0)
	{
		str[i++] = '0' + (reverse_nb % 10);
		reverse_nb /= 10;
	}
	return (str);
}

char			*ft_addstr(char *add, char *str)
{
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (str[i])
		++i;
	while (add[j])
	{
		str[i + j] = add[j];
		++j;
	}
	return (str);
}

char			*ft_adddate(char *str, char *ret)
{
	int			i;
	int			j;
	int			k;

	i = 0;
	j = 0;
	k = 0;
	while (ret[i])
		++i;
	while (str[k] != ' ')
		++k;
	++k;
	while (str[j + k])
	{
		ret[i + j] = str[j + k];
		if (str[j + k] == ':')
			str[(j + k) + 3] = '\0';
		++j;
	}
	return (ret);
}
