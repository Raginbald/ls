/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:01:36 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
#define FT_LS_H

typedef	struct		s_file
{
	char			*name;
	struct s_file	*next;
	struct s_file	*prev;
}					t_file;

typedef struct	s_info
{
	char	options[6];
	int		space_nblink;
	int		space_user;
	int		space_group;
	int		space_size;
	t_file	*files;
}			t_info;

typedef	struct	s_dirent
{
	char				read;
	struct	dirent		*data;
	struct	s_dirent	*next;
	struct	s_dirent	*prev;
}			t_dirent;

#define		OPTION_L	1
#define		OPTION_RR	2
#define		OPTION_A	3
#define		OPTION_R	4
#define		OPTION_T	5
#define		NB_OPTION	5

#define MODE(z) (( stats.st_mode & (z)) == (z))

/*-ft_addstr.c-*/
char		*ft_addnbr(int nb, char *str);
char		*ft_addchar(char c, char *str);
char		*ft_addstr(char *add, char *str);
char		*ft_adddate(char *str, char *ret);

/*-ft_readfiles.c-*/
t_dirent	*ft_getdir(const char *dir);
t_dirent	*ft_triefiles(t_info *files, t_dirent *dir);

/*--ft_list.c---*/
t_file		*ft_addfile(t_file *files, char const *file_name);
void		*ft_dellhidlist(t_dirent *file);
void		*ft_invlist(t_dirent *files);
t_file		*ft_addfileher(t_file *rep, char *name, char *parent);

/*--ft_files.c---*/
t_info		*ft_getfiles(t_info *info, int ac, char **av);
int			ft_isfile(char const *str, char opt);

/*---ft_lib.c----*/
char		*ft_getpath(char *s1, char *s2, char opt);
void		ft_putchar(char c);
int			ft_strcmp(char *s1, char *s2);
void		ft_putnbr(int nb);
void		ft_putstr(char const *str);
void		ft_free(void *ptr);
char		*ft_getstr(const char *str);
void		*ft_memalloc(unsigned int size);
int			ft_strlen(char const *str);

/*----ft_ls.c----*/
int			ft_ls(int ac, char **av);

/*ft_cleanliste.c*/
char		**ft_addspace(int size, char **list, int n_col);
void		ft_cleanstr(char *str);
char		**ft_columliste(char **ret);
char		*ft_addspacestr(char *str, int nb_space, int index);

/*-ft_writels.c-*/
int			ft_getsizecol(int index, char *str);
void		ft_printnamefile(t_info *info, t_dirent *rep);

/*--ft_option.c--*/
t_info		*ft_cleanoptions(t_info *info);
int			ft_isvalcharoption(char c);
int			ft_isoption(char const *str);
int			ft_haveoption(t_info *info, char c);
t_info		*ft_getoptions(t_info *info, int ac, char **av);

#endif /*FT_LS_H*/
