/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_files.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 17:07:16 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 18:51:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		<stdio.h>

#include		<unistd.h>
#include		<sys/types.h>
#include		<sys/stat.h>
#include		<fcntl.h>

#include		"ft_ls.h"

t_info			*ft_getfiles(t_info *info, int ac, char **av)
{
	int			i;

	i = 0;
	while (ac > ++i)
		if (ft_isfile(av[i], 1))
			info->files = ft_addfile(info->files, av[i]);
	while (info->files->prev)
		info->files = info->files->prev;
	return (info);
}

int				ft_isfile(char const *str, char opt)
{
	int			fd;

	fd = open(str, O_RDONLY);
	if (fd > 0)
	{
		close(fd);
		return (1);
	}
	if (opt == 1 && str[0] != '-')
	{
		ft_putstr("Le repertoire < ");
		ft_putstr(str);
		ft_putstr(" > n'existe pas!\n");
	}
	close(fd);
	return (0);
}
